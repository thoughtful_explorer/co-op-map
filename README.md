# Co-op Map

This tool was previously designed to extract Co-op locations from the Cooperative Grocer Network (CGN) website, but that website is now defunct. 
The tool now instead extracts North American cooperative grocer locations from the official [Grocery Story website](https://grocerystory.coop/food-co-op-directory) and and places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for calling the API
    * Simplekml for easily building KML files
    * JSON module for JSON-based geodata
* Also of course depends on official [Grocery Story website](https://grocerystory.coop/food-co-op-directory).
