#!/usr/bin/python3

import requests
import simplekml
import json

#Set filename/path for KML file output
kmlfile = "coop.kml"
#Set KML schema name
kmlschemaname = "coop"
#Set api URL
apiURL = "https://api.storepoint.co/v1/163800a1217c39/locations?rq"
#Get the API response and JSONify it
response = requests.get(apiURL)
response = response.json()

# Example JSON
# {"success":true,"results":{"locations":[{"custom_fields":"[]","description":null,"email":null,"extra":null,"extra2":null,"extra3":null,"facebook":null,"friday":null,"id":41774859,"image_url":null,"instagram":null,"loc_lat":41.089546,"loc_long":-85.153231,"monday":null,"name":"3 Rivers Food Co-op","phone":"","priority":null,"saturday":null,"streetaddress":"1612 Sherman Blvd, Ft. Wayne, Indiana 46808","sunday":null,"tags":"","thursday":null,"tuesday":null,"twitter":null,"website":"http:\/\/www.3riversfood.coop\/","wednesday":null,"color":null},

#Initialize kml object
kml = simplekml.Kml()
#Add schema, which is required for a custom address field
schema = kml.newschema(name=kmlschemaname)
schema.newsimplefield(name="address",type="string")

#Iterate through all the JSON points used by leaflet
for point in response["results"]["locations"]:
    #Get store attributes - this is very nice, clean data
    storename = point["name"]
    lat = point["loc_lat"]
    lng = point["loc_long"]
    storeaddress = point["streetaddress"] 

    #Create the point name and description in the kml
    point = kml.newpoint(name=storename,description="market")
    #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
    point.extendeddata.schemadata.schemaurl = kmlschemaname
    simpledata = point.extendeddata.schemadata.newsimpledata("address",storeaddress)
    #Finally, add coordinates to the feature
    point.coords=[(lng,lat)]
#Save the final KML file
kml.save(kmlfile)
